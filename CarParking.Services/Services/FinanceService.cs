﻿using System.Collections.Generic;
using System.Linq;
using CarParking.Shared.Contracts;
using CarParking.Shared.Contracts.Services;
using CarParking.Shared.Entities;
using CarParking.Shared.Entities.Abstracts;

namespace CarParking.Services
{
    public class FinanceService : IFinanceService
    {
        public decimal GetParkingBalance()
        {
            return ParkingStorage.Balance;
        }

        public decimal GetParkingBalanceInLastMinute(IList<IParkingTransaction> transactions)
        {
            return transactions == null || !transactions.Any() ? default(decimal) : transactions.Select(o => o.TransactionAmount).Sum();
        }

        public decimal GetVehicleBalance(Vehicle vehicle)
        {
            return vehicle.Balance;
        }

        public void AddVehicleBalance(Vehicle vehicle, decimal value)
        {
            vehicle.Balance += value;
        }
    }
}