﻿using System.Collections.Generic;
using CarParking.Shared.Contracts;
using CarParking.Shared.Contracts.Services;

namespace CarParking.Services
{
    public class TransactionService : ITransactionService
    {
        public IList<IParkingTransaction> TransactionsInLastMinute { get; set; }

        public TransactionService()
        {
            TransactionsInLastMinute = new List<IParkingTransaction>();
        }
        public IEnumerable<IParkingTransaction> GetTransactionsInLastMinute()
        {
            return TransactionsInLastMinute;
        }
    }
}