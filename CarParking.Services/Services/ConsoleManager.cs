﻿using System;
using CarParking.Shared.Contracts.Services;
using CarParking.Shared.Enums;
using CarParking.Shared.Events;
using CarParking.Shared.Extensions;

namespace CarParking.Services
{
    public class ConsoleManager : IConsoleManager
    {
        public event EventHandler<AddVehicleToParkingEvent> OnAddVehicleToParking;
        public event EventHandler<GetParkingBalanceEvent> OnGetParkingBalance;
        public event EventHandler<GetFreePlacesEvent> OnGetFreePlaces;
        public event EventHandler<PrintTransactionsInLastMinuteEvent> OnPrintTransactionsInLastMinute;
        public event EventHandler<PrintTransactionsHistoryEvent> OnPrintTransactionsHistory;
        public event EventHandler<PrintVehiclesCollectionEvent> OnPrintVehiclesCollection;
        public event EventHandler<RemoveVehicleEvent> OnRemoveVehicleEvent;
        public event EventHandler<GetBalanceInLastMinuteEvent> OnGetBalanceInLastMinute;
        public event EventHandler<AddVehicleBalanceEvent> OnAddVehicleBalance;

        public void StartConsole()
        {
            PrintWelcomeMessage();
            string key = null;
            do
            {
                key = Console.ReadLine();

                switch (key)
                {
                    case "exit": break;

                    case "help":
                        PrintHelpMessage();
                        break;
                    case "1":
                        PrintParkingBalance();
                        break;
                    case "2":
                        PrintBalanceInLastMinute();
                        break;
                    case "3":
                        PrintFreeParking();
                        break;
                    case "4":
                        PrintLastMinuteTransactions();
                        break;
                    case "5":
                        PrintAllTransactions();
                        break;
                    case "6":
                        PrintAllVehiclesOnParking();
                        break;
                    case "7":
                        TryAddVehicleToParking();
                        break;
                    case "8":
                        TryRemoveVehicle();
                        break;
                    case "9":
                        TryAddBalanceToVehicle();
                        break;
                    default:
                        PrintErrorMessage();
                        break;
                }

            } while (key != "exit");

            PrintByeMessage();
        }

        private void TryAddBalanceToVehicle()
        {
            PhoneNumberQuestion();
            var phone = Console.ReadLine();
            PrintCoinAmountQueastion();
            var value = Console.ReadLine();
            if (decimal.TryParse(value, out var result))
            {
                OnAddVehicleBalance?.Invoke(this, new AddVehicleBalanceEvent(phone, result));
            }
            else
            {
                PrintErrorMessage();
            }
        }

        private void PrintCoinAmountQueastion()
        {
            Console.WriteLine(@"Please, enter amount od maoney");
        }

        private void TryRemoveVehicle()
        {
            PhoneNumberQuestion();
            var phone = Console.ReadLine();
            OnRemoveVehicleEvent?.Invoke(this, new RemoveVehicleEvent(phone));
        }

        private void TryAddVehicleToParking()
        {
            AddingVehicleSubmenu();

            while (true)
            {
                var key = Console.ReadLine();
                if (int.TryParse(key, out var typeNumber) && typeNumber >= 0 && typeNumber < 5)
                {
                    if (typeNumber == 0)
                        return;

                    PhoneNumberQuestion();
                    var phone = Console.ReadLine();
                    var vehicle = ((VehicleEnum)typeNumber).CreateVehicle(phone);
                    OnAddVehicleToParking?.Invoke(this, new AddVehicleToParkingEvent(vehicle));
                    break;
                }
            }

            Console.WriteLine("--- Success ---");
        }

        private void PrintAllVehiclesOnParking()
        {
            OnPrintVehiclesCollection?.Invoke(this, new PrintVehiclesCollectionEvent());
        }

        private void PrintAllTransactions()
        {
            OnPrintTransactionsHistory?.Invoke(this, new PrintTransactionsHistoryEvent());
        }

        private void PrintLastMinuteTransactions()
        {
            OnPrintTransactionsInLastMinute?.Invoke(this, new PrintTransactionsInLastMinuteEvent());
        }

        private void PrintFreeParking()
        {
            OnGetFreePlaces?.Invoke(this, new GetFreePlacesEvent());
        }

        private void PrintBalanceInLastMinute()
        {
            OnGetBalanceInLastMinute?.Invoke(this, new GetBalanceInLastMinuteEvent());
        }

        private void PrintParkingBalance()
        {
            OnGetParkingBalance?.Invoke(this, new GetParkingBalanceEvent());
        }

        private void PrintErrorMessage()
        {
            PrintDashes();
            Console.WriteLine(@"Sorry. You have written incorrect keyword.");
            Console.WriteLine(@"Please, try to use 'help' keyword for help tutorial.");
            PrintDashes();
        }

        private void PrintHelpMessage()
        {
            PrintDashes();
            Console.BackgroundColor = ConsoleColor.Magenta;
            Console.WriteLine(@"For using our system try next commands:");
            Console.WriteLine(@"1 - Get Parking balance");
            Console.WriteLine(@"2 - Get Parking balance in last minute");
            Console.WriteLine(@"3 - Get free parking count");
            Console.WriteLine(@"4 - Show transactions in last minute");
            Console.WriteLine(@"5 - Show transaction history");
            Console.WriteLine(@"6 - Show all vehicles in parking");
            Console.WriteLine(@"7 - Add vehicle to parking");
            Console.WriteLine(@"8 - Remove vehicle from parking");
            Console.WriteLine(@"9 - Add vehicle balance");
            Console.WriteLine(@"help - Show help");
            Console.WriteLine(@"exit - shut down app");
            PrintDashes();
        }

        private void PrintByeMessage()
        {
            Console.WriteLine("Program has finished");
        }

        private void PrintWelcomeMessage()
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            PrintDashes();
            Console.WriteLine(@"---Welcome to 'Parking System'---");
            Console.WriteLine(@"Use for read help tutorial next keyword 'help'");
            PrintDashes();
        }

        private void PrintDashes()
        {
            Console.WriteLine(@"-----------------------------------------------------");
        }

        private void AddingVehicleSubmenu()
        {
            PrintDashes();
            Console.WriteLine(@"Chose vehicle type");
            Console.WriteLine(@"1 - Bike");
            Console.WriteLine(@"2 - Car");
            Console.WriteLine(@"3 - Bus");
            Console.WriteLine(@"4 - Truck");
            Console.WriteLine(@"0 - Return to parent menu");
            PrintDashes();
        }

        private void PhoneNumberQuestion()
        {
            PrintDashes();
            Console.WriteLine(@"Please, enter your phone number...");
            PrintDashes();
        }
    }
}