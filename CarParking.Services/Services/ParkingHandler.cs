﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using CarParking.Shared;
using CarParking.Shared.Contracts;
using CarParking.Shared.Contracts.Services;
using CarParking.Shared.Entities;
using CarParking.Shared.Entities.Abstracts;
using CarParking.Shared.Events;
using CarParking.Shared.Exceptions;
using CarParking.Shared.Extensions;

namespace CarParking.Services
{
    public class ParkingHandler : IParkingHandler
    {
        private readonly ITransactionService _transactionService;
        private readonly ILoggingService _loggingService;
        private readonly IFinanceService _financeService;
        public IParkingStorage ParkingStorage { get; }

        public IConsoleManager ConsoleManager { get; }
        public ParkingHandler(IConsoleManager consoleManager,
            ITransactionService transactionService,
            ILoggingService loggingService,
            IFinanceService financeService)
        {
            _transactionService = transactionService;
            _loggingService = loggingService;
            _financeService = financeService;
            ConsoleManager = consoleManager;
            ParkingStorage = Shared.Entities.ParkingStorage.CreateParkingStorage();
            DoParking().ConfigureAwait(false);

            ConsoleManager.OnAddVehicleToParking += AddVehicleToParking;
            ConsoleManager.OnAddVehicleBalance += AddVehicleBalance;
            ConsoleManager.OnGetBalanceInLastMinute += PrintParkingbalanceLastMinute;
            ConsoleManager.OnGetParkingBalance += PrintTotalBalance;
            ConsoleManager.OnGetFreePlaces += PrintFreePlacesCount;
            ConsoleManager.OnPrintTransactionsHistory += PrintTransactionsHistory;
            ConsoleManager.OnPrintVehiclesCollection += PrintVehiclesInParking;
            ConsoleManager.OnRemoveVehicleEvent += RemoveVehicleFromParking;
            ConsoleManager.OnPrintTransactionsInLastMinute += PrintTrasactionsInLastMinute;

            InitConsoleManager();
        }

        private void PrintTrasactionsInLastMinute(object sender, PrintTransactionsInLastMinuteEvent e)
        {
            _loggingService.PrintTransactions(_transactionService.TransactionsInLastMinute);
        }

        private void PrintVehiclesInParking(object sender, PrintVehiclesCollectionEvent e)
        {
            foreach (var vehicle in ParkingStorage.VehicleCollection)
            {
                Console.WriteLine($"Id= {vehicle.VehicleId} VehicleType= {vehicle.Type} Balance= {vehicle.Balance} ");
            }
        }

        private void PrintTransactionsHistory(object sender, PrintTransactionsHistoryEvent e)
        {
            _loggingService.PrintAllTransactionsFromFile();
        }

        private void PrintFreePlacesCount(object sender, GetFreePlacesEvent e)
        {
            Console.WriteLine(ParkingSettings.Capacity - ParkingStorage.VehicleCollection.Count);
        }

        private void PrintTotalBalance(object sender, GetParkingBalanceEvent e)
        {
            Console.WriteLine(_financeService.GetParkingBalance());
        }

        private void PrintParkingbalanceLastMinute(object sender, GetBalanceInLastMinuteEvent e)
        {
            var balanceInMinute = _financeService.GetParkingBalanceInLastMinute(_transactionService.TransactionsInLastMinute);
            Console.WriteLine(balanceInMinute);
        }

        private void AddVehicleBalance(object sender, AddVehicleBalanceEvent e)
        {
            var result = ParkingStorage.VehicleCollection.FirstOrDefault(o => o.OwnerPhone == e.PhoneNumber);
            if (result != null)
                result.Balance += e.CountValue;
            else
                Console.WriteLine("Your vehicle wasn't found");
        }

        private void InitConsoleManager()
        {
            ConsoleManager.StartConsole();
        }

        private async Task DoParking()
        {
            await Task.Run(StartBalance);
        }

        private async Task StartBalance()
        {
            while (true)
            {
                try
                {
                    ParkingStorage.VehicleCollection.UpdateItems(WithdrawBalance);

                    Thread.Sleep(TimeSpan.FromSeconds(ParkingSettings.PaymentTimeoutInSeconds));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    break;
                }

            }

            await Task.CompletedTask;
        }

        private Vehicle WithdrawBalance(Vehicle vehicle)
        {
            lock (ParkingStorage.VehicleCollection)
            {
                if (vehicle.Balance > vehicle.Rate)
                {
                    vehicle.Balance -= vehicle.Rate;
                    ParkingStorage.AddBalance(vehicle.Rate);
                    ProcessTransaction(vehicle.VehicleId, vehicle.Rate);

                }
                else
                {
                    var value = (vehicle.Rate * (decimal)ParkingSettings.ParkPenalty);
                    vehicle.Balance -= value;
                    ParkingStorage.AddBalance(value);
                    ProcessTransaction(vehicle.VehicleId, value);
                }

                return vehicle;
            }
        }

        private void ProcessTransaction(Guid vehicleId, decimal value)
        {
            var transaction = new ParkingTrasaction(vehicleId) { TransactionAmount = value };
            _transactionService.TransactionsInLastMinute.Add(transaction);

            _loggingService.WriteTransactionToFile(_transactionService.TransactionsInLastMinute
                .Where(o => o.TransactionTime - DateTime.Now > TimeSpan.FromMinutes(1))
                .ToList());
            _transactionService.TransactionsInLastMinute = _transactionService.TransactionsInLastMinute
                .Where(o => o.TransactionTime - DateTime.Now < TimeSpan.FromMinutes(1)).ToList();
        }

        public int GetFreePlacesCount()
        {
            return ParkingSettings.Capacity - ParkingStorage.VehicleCollection.Count;
        }

        public IList<Vehicle> GetAllVehicles()
        {
            return ParkingStorage.VehicleCollection;
        }

        private void AddVehicleToParking(object sender, AddVehicleToParkingEvent ev)
        {
            try
            {
                ParkingStorage.AddVehicleToParking(ev.Vehicle);
            }
            catch (FreeParkingPlaceException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception)
            {
                Console.WriteLine("Internal error");
            }
        }

        public void RemoveVehicleFromParking(object sender, RemoveVehicleEvent ev)
        {
            var vehicle = ParkingStorage.VehicleCollection.FirstOrDefault(o => o.OwnerPhone == ev.PhoneNumber);

            if (vehicle == null)
                Console.WriteLine("Vehicle not found");
            else
            {
                try
                {
                    ParkingStorage.RemoveVehicleFromParking(vehicle.VehicleId);
                }
                catch (VehicleNotParkedException e)
                {
                    Console.WriteLine(e);
                }
                catch (NegativeVehicleBalanceException e)
                {
                    Console.WriteLine(e);
                }
                catch (Exception)
                {
                    Console.WriteLine("Internal error");
                }
            }
        }
    }
}