﻿namespace CarParking.Shared.Enums
{
    public enum VehicleEnum : sbyte
    {
        Car,
        Truck,
        Bus,
        Bike
    }
}