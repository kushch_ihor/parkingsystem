﻿using System;

namespace CarParking.Shared.Contracts
{
    public interface IParkingTransaction
    {
        Guid Id { get; }
        DateTime TransactionTime { get; }
        Guid VehicleId { get; }
        decimal TransactionAmount { get; set; }
    }
}