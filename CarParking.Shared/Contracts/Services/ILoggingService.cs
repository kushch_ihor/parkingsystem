﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarParking.Shared.Contracts.Services
{
    public interface ILoggingService
    {
        void WriteTransactionToFile(IList<IParkingTransaction> transaction);
        Task WriteTransactionToFileAsync(IList<IParkingTransaction> transaction);
        void PrintAllTransactionsFromFile();
        Task PrintAllTransactionsFromFileAsync();
        void PrintTransactions(IList<IParkingTransaction> transactions);
    }
}