﻿using System;
using CarParking.Shared.Events;

namespace CarParking.Shared.Contracts.Services
{
    public interface IConsoleManager
    {
        event EventHandler<AddVehicleToParkingEvent> OnAddVehicleToParking;
        event EventHandler<GetParkingBalanceEvent> OnGetParkingBalance;
        event EventHandler<GetFreePlacesEvent> OnGetFreePlaces;
        event EventHandler<PrintTransactionsInLastMinuteEvent> OnPrintTransactionsInLastMinute;
        event EventHandler<PrintTransactionsHistoryEvent> OnPrintTransactionsHistory;
        event EventHandler<PrintVehiclesCollectionEvent> OnPrintVehiclesCollection;
        event EventHandler<RemoveVehicleEvent> OnRemoveVehicleEvent;
        event EventHandler<GetBalanceInLastMinuteEvent> OnGetBalanceInLastMinute;
        event EventHandler<AddVehicleBalanceEvent> OnAddVehicleBalance; 

        void StartConsole();
    }
}