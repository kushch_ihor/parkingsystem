﻿using System;

namespace CarParking.Shared.Exceptions
{
    public class NegativeVehicleBalanceException : Exception
    {
        public NegativeVehicleBalanceException(string errorMessage):base(errorMessage)
        {
        }
    }
}