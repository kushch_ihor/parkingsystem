﻿using System;

namespace CarParking.Shared.Events
{
    public class AddVehicleBalanceEvent : EventArgs
    {
        public string PhoneNumber { get; }
        public decimal CountValue { get; }
        public AddVehicleBalanceEvent(string phonenumber, decimal addingValue)
        {
            PhoneNumber = phonenumber;
            CountValue = addingValue;
        }
    }
}