﻿using System;
using CarParking.Shared.Entities.Abstracts;

namespace CarParking.Shared.Events
{
    public class AddVehicleToParkingEvent : EventArgs
    {
        public Vehicle Vehicle { get; }
        public AddVehicleToParkingEvent(Vehicle vehicle)
        {
            Vehicle = vehicle;
        }
    }
}