﻿using System;
using CarParking.Shared.Enums;

namespace CarParking.Shared.Helpers
{
    public static class VehicleRateHelper
    {
        public static decimal GetVehicleRate(VehicleEnum type)
        {
            switch (type)
            {
                case VehicleEnum.Bike:
                    return ParkingSettings.Price.BikeRate;
                case VehicleEnum.Car:
                    return ParkingSettings.Price.CarRate;
                case VehicleEnum.Bus:
                    return ParkingSettings.Price.BusRate;
                case VehicleEnum.Truck:
                    return ParkingSettings.Price.TruckRate;
                default:
                    throw new ArgumentException("Invalid type of vehicle");
            }
        }
    }
}