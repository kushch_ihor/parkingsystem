﻿using CarParking.Shared.Entities.Abstracts;
using CarParking.Shared.Enums;
using CarParking.Shared.Helpers;

namespace CarParking.Shared.Entities.Vehicles
{
    public class Car : Vehicle
    {
        public Car(string phone) : base(phone)
        {
            Type = VehicleEnum.Car;
        }

        public override VehicleEnum Type { get; }
        public override decimal Rate => VehicleRateHelper.GetVehicleRate(Type);
    }
}