﻿using CarParking.Shared.Entities.Abstracts;
using CarParking.Shared.Enums;
using CarParking.Shared.Helpers;

namespace CarParking.Shared.Entities.Vehicles
{
    public class Truck : Vehicle
    {
        public Truck(string phone) : base(phone)
        {
            Type = VehicleEnum.Truck;
        }

        public override VehicleEnum Type { get; }
        public override decimal Rate => VehicleRateHelper.GetVehicleRate(Type);
    }
}