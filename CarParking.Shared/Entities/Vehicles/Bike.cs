﻿using CarParking.Shared.Entities.Abstracts;
using CarParking.Shared.Enums;
using CarParking.Shared.Helpers;

namespace CarParking.Shared.Entities.Vehicles
{
    public class Bike : Vehicle
    {
        public Bike(string phone) : base(phone)
        {
            Type = VehicleEnum.Bike;
        }

        public override VehicleEnum Type { get; }
        public override decimal Rate => VehicleRateHelper.GetVehicleRate(VehicleEnum.Bike);
    }
}