﻿using CarParking.Shared.Entities.Abstracts;
using CarParking.Shared.Enums;
using CarParking.Shared.Helpers;

namespace CarParking.Shared.Entities.Vehicles
{
    public class Bus : Vehicle
    {
        public Bus(string phone) : base(phone)
        {
            Type = VehicleEnum.Bus;
        }

        public override VehicleEnum Type { get; }
        public override decimal Rate => VehicleRateHelper.GetVehicleRate(Type);
    }
}