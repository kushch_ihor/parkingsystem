﻿using System.Text.RegularExpressions;

namespace CarParking.Shared.Extensions
{
    public static class StringExtensions
    {
        private static Regex _phoneRegex = new Regex(@"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}");

        public static bool IsValidPhoneNumber(this string phoneNumber)
        {
            return _phoneRegex.IsMatch(phoneNumber);
        }
    }
}