﻿using System;
using CarParking.Shared.Entities.Abstracts;
using CarParking.Shared.Entities.Vehicles;
using CarParking.Shared.Enums;

namespace CarParking.Shared.Extensions
{
    public static class VehicleEnumExtensions
    {
        public static Vehicle CreateVehicle(this VehicleEnum type, string phone)
        {
            switch (type)
            {
                case VehicleEnum.Car:
                    return new Car(phone);
                case VehicleEnum.Bike:
                    return new Bike(phone);
                case VehicleEnum.Bus:
                    return new Bus(phone);
                case VehicleEnum.Truck:
                    return new Truck(phone);
                default:
                    throw new ArgumentException();
            }
        }
    }
}