﻿namespace CarParking.Shared
{
    public static class ParkingSettings
    {
        public static decimal Balance = 0;
        public static int Capacity = 10;
        public static int PaymentTimeoutInSeconds = 5;
        public static double ParkPenalty = 2.5;

        public static class Price
        {
            public static decimal CarRate = 2.0M;
            public static decimal TruckRate = 5.0M;
            public static decimal BusRate = 3.5M;
            public static decimal BikeRate = 1.0M;
        }
    }
}