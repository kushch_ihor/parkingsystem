﻿using CarParking.Services;
using CarParking.Shared.Contracts.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CarParking
{
    class Program
    {
        static void Main()
        {

            var serviceProvider = new ServiceCollection()
                .AddSingleton<ILoggingService, LoggingService>()
                .AddSingleton<IFinanceService, FinanceService>()
                .AddSingleton<ITransactionService, TransactionService>()
                .AddSingleton<IParkingHandler, ParkingHandler>()
                .AddScoped<IConsoleManager, ConsoleManager>()
                .BuildServiceProvider();


            var handler = serviceProvider.GetService<IParkingHandler>();

        }
    }
}
